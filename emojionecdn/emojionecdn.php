<?php
/**
 * Name: EmojiOne CDN
 * Description: EXPERIMENTAL! Load EmojiOne via jsDelivr
 * Version: 1.1
 * Author: Yuki <https://gme.one/channel/yahananxie>
 * Maintainer: Yuki <https://gme.one/channel/yahananxie>
 */

function emojionecdn_install() {
	register_hook('page_content_top', 'addon/emojionecdn/emojionecdn.php', 'emojionecdn_insert');
}

function emojionecdn_uninstall() {
	unregister_hook('page_content_top', 'addon/emojionecdn/emojionecdn.php', 'emojionecdn_insert');
}

function emojionecdn_insert() {
	App::$page['htmlhead'] .= '<script data-cfasync="false" src="https://cdn.jsdelivr.net/npm/emojione@4/lib/js/emojione.min.js"></script>' . "\r\n";
	App::$page['htmlhead'] .= '<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/emojione@4/extras/css/emojione.min.css" type="text/css" media="all" />' . "\r\n";
	//App::$page['htmlhead'] .= '<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/emojione@4/extras/css/emojione-awesome.min.css" type="text/css" media="all" />' . "\r\n";
}
