# EmojiOne CDN
EXPERIMENTAL! EmojiOne via jsDelivr CDN addon for Hubzilla.

This addon inserts the following in your Hubzilla's HTML header section.
```
<script src="https://cdn.jsdelivr.net/npm/emojione@3/lib/js/emojione.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/emojione@3/extras/css/emojione.min.css">
```

This way, whenever there is a Unicode Emoji, it is automatically replaced by an EmojiOne design.

## Version
* Hubzilla Addon: 1.1
* EmojiOne: 4 (always uses the latest update in the v4 branch via jsDelivr)
* Unicode Emoji support: 11

## Free License
A free license is the perfect way to get emoji integrated into your project quickly and easily. Free licenses are intended for digital use of our signature emoji set, with attribution. Those requiring vector graphics for a project should check out our premium license.

See: https://www.emojione.com/licenses/free

Updated on August 27th, 2018

* Free licenses are available for personal and commercial digital use of EmojiOne icons
* Free license allows for use of non-vector (PNG) icons, up to 128px
* Free license requires proper attribution (commercial use only)
* Free license does not cover commercial promotions or advertising
* Free license does not cover merchandise or printed material
* May not redistribute assets
* May not include icons in open source projects
* EmojiOne is not liable for any damages caused by the use of our icons. All icons are provided “as is” without warranty. [Contact us] if you require warranties and/or indemnification.

### What's Allowed
Our free license comes as-is, without warranties, and is not intended for consumer goods.

* Dress up your apps/sites, or integrate emoji into your content as a secondary component
* Use emoji to enhance your content, including video or social media
* Use EmojiOne icons as the primary or secondary emoji set for your users
* Non-commercial use, attribution is always required

### Not Allowed
If any of the following apply, a premium or enterprise license is required.

* Commercial promotions or advertising, both digital and print
* Consumer goods or merchandise of any kind
* Company stationery, business cards, packaging, or printing of any kind
* May not be used to create a company logo
* May not use assets as primary component of your digital application

## How to Attribute
If you're using our emoji for non-personal use, here are some suggested ways to link back to us.

* Emoji icons provided free by [EmojiOne]
* Emoji on this page provided free by [EmojiOne]
* Emoji icons supplied by [EmojiOne]
* Thanks to [EmojiOne] for providing free emoji icons

## Attribution Placement
Suggested placement for a variety of uses. If unable to attribute, upgrade to our [premium emoji license].

* In the footer of every page where emoji are shown
* Credited directly below content that contains our emoji
* In the product description on youtube, vimeo, etc
* For social media content, please tag us somewhere in the post

[Contact us]: https://www.emojione.com/contact "Contact"
[premium emoji license]: https://www.emojione.com/licenses/premium "Premium License"
[EmojiOne]: https://www.emojione.com "EmojiOne"
