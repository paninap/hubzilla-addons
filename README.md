# Paninap Hubzilla Addons
Addons for Hubzilla by Paninap Services.

These addons are intended for personal use only, specifically on the [gme.one™ Hubzilla Fediverse] hub/instance/pod. Feel free to use these addons at your own risk.

## Installation and Updating
You might need to add ```sudo```.

1. Enter this in your Hubzilla root: ```util/add_addon_repo https://bitbucket.org/paninap/hubzilla-addons.git paninap_addons```
2. To update, type this while in  your Hubzilla root: ```util/update_addon_repo paninap_addons```

## Addons List
* EmojiOne CDN - experimental!
* Twemoji
* Twemoji CDN - deprecated; deleted from repository


[gme.one™ Hubzilla Fediverse]: https://gme.one "gme.one™ Hubzilla Fediverse"
