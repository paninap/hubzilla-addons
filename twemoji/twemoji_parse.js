/**
 * Separate file to make the addon CSP (Content-Security Policy) friendly.
 */

/**
 * Some additional settings available from Twemoji JS.
 * We can probably add an addon setting to let admins choose PNG or SVG.
 * Also note, the admin might also need disable SVG blocking in Hubzilla's admin setting.
 *
 * //twemoji.base: '/path/emojis/';         // default: https://twemoji.maxcdn.com/2/
 * //twemoji.ext = '.svg';                  // default: .png
 * //twemoji.size = 'svg';                  // default: 72x72
 * //twemoji.className = 'smiley emoji';    // default: emoji
 */

/**
 * NOTE: When Twemoji is loaded in Hubzilla, the scripts below either
 *   randomly works or not at all. I have no idea why.
 */

/* Source: Twemoji instructions */
/* Works in Firefox randomly but not in Chrome */
/*
window.onload = function() {
	twemoji.parse( document.body, {
		base: 'addon/twemoji/emojis/',
		callback: function(iconId, options) {
			return options.base + iconId + options.ext;
		}
	});
}
*/

/* Source: https://stackoverflow.com/a/40310653/306674 */
/* Works in Firefox and Chrome... randomly */
function onReady(insertTwemoji) {
	var readyStateCheckInterval = setInterval(function() {
		if (document && document.readyState === 'complete') { // Or 'interactive'
			clearInterval(readyStateCheckInterval);
			insertTwemoji();
		}
	}, 10);
}
// use like
onReady(function() {
	twemoji.parse( document.body, {
		base: 'addon/twemoji/emojis/',
		callback: function(iconId, options) {
			return options.base + iconId + options.ext;
		}
	});

});

/* Not working in Firefox and chrome */
/*
document.addEventListener( 'DOMContentLoaded',
	function() {
		twemoji.parse( document.body, {
			base: 'addon/twemoji/emojis/',
			callback: function(iconId, options) {
				return options.base + iconId + options.ext;
			}
		});
	}
);
*/

/* jQuery method also not working in Firefox and Chrome
/*
$(document).ready(function() {
	twemoji.parse( document.body, {
		base: 'addon/twemoji/emojis/',
		callback: function(iconId, options) {
			return options.base + iconId + options.ext;
		}
	});
});
*/
