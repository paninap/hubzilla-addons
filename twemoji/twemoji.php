<?php

/**
 * Name: Twemoji
 * Description: Twemoji 11.1 based on Unicode Emoji 11
 * Version: 11.1.1
 * Author: Based on the emojione Hubzilla addon
 * Maintainer: Yuki <https://gme.one/channel/yuki>
 */

function twemoji_load() {
	register_hook('page_content_top', 'addon/twemoji/twemoji.php', 'twemoji_insert');
	//register_hook('prepare_body', 'addon/twemoji/twemoji.php', 'twemoji_prepare_body', 10);
	\Zotlabs\Extend\Hook::register('smilie','addon/twemoji/twemoji.php', [ '\\Twemoji' , 'smilie' ]);
}

function twemoji_unload() {
	unregister_hook('page_content_top', 'addon/twemoji/twemoji.php', 'twemoji_insert');
	//unregister_hook('prepare_body', 'addon/twemoji/twemoji.php', 'twemoji_prepare_body');
	\Zotlabs\Extend\Hook::unregister('smilie','addon/twemoji/twemoji.php', [ '\\Twemoji' , 'smilie' ]);
}

function twemoji_insert() {
	// Let's insert the Twemoji JS
	App::$page['htmlhead'] .= '<script src="' . z_root() . '/addon/twemoji/twemoji.min.js' . '" defer="defer"></script>' . "\r\n";

	// Add some CSS for emoji class to resize the emoji and to override the box-shadow
	App::$page['htmlhead'] .= '<link rel="stylesheet" href="' . z_root() . '/addon/twemoji/twemoji.css' . '" type="text/css" media="all" />' . "\r\n";

	// Let's parse the document and replace Unicode Emojis with Twemoji images
	App::$page['content'] .= '<script src="' . z_root() . '/addon/twemoji/twemoji_parse.js' . '" defer="defer"></script>' . "\r\n";
}

/*
function twemoji_prepare_body() {

}
*/

class Twemoji {
	static $listing = null;
	static public function smilie(&$x) {
		if(! self::$listing)
			self::$listing = json_decode(@file_get_contents('addon/twemoji/emoji.json'),true);

		if(self::$listing) {
			foreach(self::$listing as $lv) {
				if(strpos( ':' . $lv['short_name'] . ':', ':tone') === 0)
					continue;
				$x['texts'][] = ':' . $lv['short_name'] . ':';
				$x['icons'][] = '<img class="smiley emoji" src="addon/twemoji/emojis/' . $lv['image'] . '" alt="' . $lv['name'] . '" />';
			}
		}
	}
}
