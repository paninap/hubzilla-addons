# Twemoji
Twemoji addon for Hubzilla.

## About
This addon replaces Unicode Emojis with images to ensure proper display of Unicode Emojis in your instance. There are two common ways emojis are entered by users:

1. Through :emoji_codes: like :joy: (```:joy:```); :smile: (```:smile:```); :flag-ph: (```:flag-ph:```); :grin: (```:grin:```); :heart: (```:heart:```); :selfie: (```:selfie:```). (This method is very limited and inconsistent across platforms.)
2. Through direct input, either by using a keyboard app with emoji support (usually in Android and iOS); or by copy-paste (see: [EmojiCopy]). 🧝🏼‍♀️ 🧛🏼‍♀️ 💇🏼‍♂️ 💝 🇵🇭 🤳🏼

### On :emoji_codes:
This addon uses the emoji.json file from https://github.com/iamcal/emoji-data which in turn was based from the official Unicode Emoji text document. The Emoji Names listed in this document are the ones used as :emoji_codes: (alternate names not used).

If you see plain :emoji_codes: most likely it is not the standard primary name for that Unicode Emoji or it is new. If the latter, Twemoji has to release an update with support for the new Unicode Emojis and only then this addon will be updated.

## Version
* Hubzilla Addon: 11.1.1
* Twemoji: 11.1
* Unicode Emoji support: 11

To make it easier to track, this addon follows the versioning of Twemoji: Unicode_Version.Twemoji_Update.Hubzilla_Addon_version. Version 11.1.1 then is Unicode 11, Twemoji 11.1, Hubzilla addon 11.1.1.

## Attribution
As an open source project, attribution is critical from a legal, practical and motivational perspective in our opinion. The graphics are licensed under the CC-BY 4.0 which has a pretty good guide on best practices for attribution.

However, we consider the guide a bit onerous and as a project, will accept a mention in a project README or an 'About' section or footer on a website. In mobile applications, a common place would be in the Settings/About section (for example, see the mobile Twitter application Settings->About->Legal section). We would consider a mention in the HTML/JS source sufficient also.

## Licensing
Copyright 2018 Twitter, Inc and other contributors

Code licensed under the MIT License: http://opensource.org/licenses/MIT

Graphics licensed under CC-BY 4.0: https://creativecommons.org/licenses/by/4.0/


[EmojiCopy]: https://www.emojicopy.com "EmojiCopy"
